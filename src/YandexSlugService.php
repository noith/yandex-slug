<?php
namespace Noith\YandexSlug;

/**
 * Description of YandexSlug
 *
 * @author noith
 */
class YandexSlugService
{

    protected $vowels = [
        'А' => 'A',
        'Е' => 'E',
        'Ё' => 'Yo',
        'И' => 'I',
        'О' => 'O',
        'У' => 'U',
        'Ы' => 'Y',
        'Э' => 'E',
        'Ю' => 'Yu',
        'Я' => 'Ya',
    ];
    protected $letters = [
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Ж' => 'Zh',
        'З' => 'Z',
        'Й' => 'Y',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'Ф' => 'F',
        'Х' => 'Kh',
        'Ц' => 'Ts',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Sch',
        'Ъ' => '',
        'Ь' => '',
    ];

    public function __construct($replacement = [])
    {
        $this->letters = [...$this->vowels, ...$this->letters, ...$replacement];
    }

    public function slugify($str, $delimiter = '-')
    {
        $str = mb_convert_case(trim($str), MB_CASE_UPPER);
        //Левые символы меняем на - или другой разделитель
        $str = preg_replace('/[^A-ZА-ЯЁ0-9-]+/', $delimiter, $str);
        //Убираем дубли разделителей
        $str = str_replace($delimiter . $delimiter, $delimiter, $str);
        $str = str_replace($delimiter . $delimiter, $delimiter, $str);
        //Убираем разделитель с начала и конца
        $str = trim($str, $delimiter);
        $words = explode($delimiter, $str);
        $wordsNew = [];
        foreach ($words as $word) {
            $wordsNew[] = $this->slugifyWord($word);
        }
        return implode($delimiter, $wordsNew);
    }

    public function slugifyWord($str)
    {
        $str = mb_strtoupper(trim($str));
        $str = str_replace('ЪЕ', 'ye', $str);
        //Если начинается с Е
        if (mb_substr($str, 0, 1) === "Е") {
            $str = "ye" . mb_substr($str, 1);
        }
        //Если кончается на ЫЙ и ИЙ
        $ending = mb_substr($str, -2);
        if ($ending == "ЫЙ" || $ending == "ИЙ") {
            $str = mb_substr($str, 0, -2) . 'iy';
        }
        //Если Е после гласных
        for ($i = 0; $i < mb_strlen($str); $i++) {
            $letter = mb_substr($str, $i, 1);
            if ($letter == "Е") {
                $prev_letter = mb_substr($str, ($i - 1), 1);
                if (array_key_exists($prev_letter, $this->vowels)) {
                    $str = mb_substr($str, 0, $i) . 'ye' . mb_substr($str, $i + 1);
                }
            }
        }
        //Стандартная замена
        $str = str_replace(array_keys($this->letters), array_values($this->letters), $str);
        return strtolower($str);
    }

}
